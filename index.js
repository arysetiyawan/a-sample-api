// Import required modules
const express = require('express');
const bodyParser = require('body-parser');

// Create an instance of Express
const app = express();

// Middleware to parse JSON body of POST requests
app.use(bodyParser.json());

// Sample data to store POST request data
let postData = [];

// Route to handle GET requests
app.get('/', (req, res) => {
    res.send('Hello, this is a GET request!');
});

// Route to handle POST requests
app.post('/data', (req, res) => {
    // Access the POST request body
    const data = req.body;
    
    // Store the data
    postData.push(data);
    
    // Respond with a confirmation message
    res.send('Data received and stored successfully!');
});

// Route to retrieve stored POST request data
app.get('/data', (req, res) => {
    res.json(postData);
});

// Start the server
const port = 3030; // You can change this port number if needed
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});
